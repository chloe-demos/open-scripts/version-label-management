const axios = require('axios');
const fs = require('fs');

// Function to increment label value and update the label
async function incrementLabel() {
    try {        // Make GET request to fetch Epic details
        const response = await axios.get('https://gitlab.com/api/v4/groups/' + process.env.GROUP_ID + '/epics', {
            headers: {
                'PRIVATE-TOKEN': process.env.GLPAT_LABELS
            }
        });

        // Iterate over each Epic
        for (const epic of response.data) { 
        // Check if the label 'increment-version' exists
            if (epic.labels.includes('increment-version')) {
                // Extract and increment label value
                let labelValue = epic.labels.find(label => label.startsWith('version::')).split('::')[1];
                let incrementedValue = parseInt(labelValue) + 1;

            // Construct new label value
            let newLabelValue = `version::${incrementedValue}`;

            // Make PUT request to update the label
            await axios.put('https://gitlab.com/api/v4/groups/' + process.env.GROUP_ID + '/epics/' + epic.iid, {
                labels: [newLabelValue, 'CovTest::To-Be-Reviewed', 'CovRationale::To-be-reviewed', 'CovReq::To-be-Reviewed', 'Status::Draft'],
                remove_labels: ['increment-version']
            }, {
                headers: {
                    'PRIVATE-TOKEN': process.env.GLPAT_LABELS
                }
        });

                console.log('Label incremented successfully for Epic: ' + epic.title);
            } else {
                console.log('Label increment-version not found for Epic: ' + epic.title + ' No incrementation performed');
            }
        }  
    } catch (error) {
        console.error('Error:', error.message);
    }
}

// Call the function to increment label
incrementLabel();


