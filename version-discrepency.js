const axios = require('axios');

async function versionDiscrepency() {
    try {
        // Fetch the list of Epics
        const response = await axios.get('https://gitlab.com/api/v4/groups/' + process.env.GROUP_ID +'/epics', {
            headers: {
                'PRIVATE-TOKEN': process.env.GLPAT_LABELS
            }
        });

        // Iterate over each Epic
        for (const childEpic of response.data) {
            // verify if the epic is a child epic
            const parentId = childEpic.parent_iid;
            
            if (parentId !== null) {
                // Get child Epic's version label
                let childLabelResponse = childEpic.labels
                let childVersionLabel = childEpic.labels.find(label => label.startsWith('ReqVersion::'));
                if (childVersionLabel!=null) {
                    childVersionLabel = childVersionLabel.split('::')[1];
                }
               
                // Get parent Epic's labels
                const parentLabelResponse = await axios.get('https://gitlab.com/api/v4/groups/' + process.env.GROUP_ID + '/epics/' + parentId, {
                    headers: {
                        'PRIVATE-TOKEN': process.env.GLPAT_LABELS
                    }
                });
                const parentVersionLabel = parentLabelResponse.data.labels.find(label => label.startsWith('version::')).split('::')[1];

                // Compare version labels
                if (childVersionLabel !== parentVersionLabel && childVersionLabel !== undefined) {
                    // Add "Impact to be Reviewed" label to child Epic
                    console.log(childVersionLabel)
                    await axios.put('https://gitlab.com/api/v4/groups/' + process.env.GROUP_ID + '/epics/' + childEpic.iid, {
                        labels: ['impact-to-review']
                    }, {
                        headers: {
                            'PRIVATE-TOKEN': process.env.GLPAT_LABELS
                        }
                    });
                    console.log(`Adding "impact-to-review" label to Child Epic ${childEpic.title}`);
                }
                else if (childVersionLabel == undefined) {
                    console.log(`Child Epic ${childEpic.title} has no version assigned`);
                }
                else {
                    console.log(`Child Epic ${childEpic.title} has the same version as its parent Epic`);
                    console.log('childVersionLabel:', childVersionLabel);
                }
            }
            else {
                console.log(`Child Epic ${childEpic.title} has no parent Epic`);
            }
        }
    } catch (error) {
        console.error('Error:', error.message);
    }
}

// Call the function to check the version discrepency
versionDiscrepency();


