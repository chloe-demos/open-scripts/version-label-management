This project provides two scripts to help manage epic and sub epics version labels. 

The scripts are triggered by the pipeline that can be run on schedule. 

version-increment will increment the label version::* 

version-discrepency will flag any discrepency between the parent epic and the child epic version label 

This project requires two environment variables:
- GLPAT_LABELS - the group access token with api (read / write) permission
- GROUP_ID - the group ID where Epics are located 